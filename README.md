# KotlinMvvmForecatSampleApp

This project is a sample Kotlin android project with best practices using MVVM architecture and also some Kotlin best librairies for Dépendency Injection, Background Task and Jobs. This project also use retroffit and Room librairies.

# Some librairies use in this project

 *  Android Jetpack Navigation
 *  Room librairy for database management
 *  Lifecycle extensions for MVVM architecture
 *  Kotlin Coroutines for background tasks and jobs 
 *  Kodein librairy for dependency injection
 *  Threetenabp librairy for Zone Date best management
 *  Retrofit for REST API to send request to remote server
 *  Groupie Recyclerview for more flexible and esay management of recyclerView
 
